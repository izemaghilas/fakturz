<?php

namespace App\DataFixtures;

use App\Entity\Product;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class ProductFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $product = new Product();
        $product->setName('Design responsive');
        $product->setNote(null);
        $product->setUnit('h');
        $product->setTva(10);
        $product->setPrice(1000);
        $manager->persist($product);
        
        $product = new Product();
        $product->setName('Référencement SEO');
        $product->setNote(null);
        $product->setUnit('h');
        $product->setTva(10);
        $product->setPrice(500);
        $manager->persist($product);

        $manager->flush();
    }
}
