<?php

namespace App\DataFixtures;

use App\Entity\Company;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class CompanyFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        $owner = $manager->getRepository(User::class)->findOneBy(['email'=>'dev@user']);

        $company = new Company();
        $company->setName('Fakturz');
        $company->setLegalStatus('SARL');
        $company->setSiret('55217863900132');
        $company->setAddress('45 rue des Saints Pères');
        $company->setCity('Paris');
        $company->setPostalCode('75006');
        $company->setCountry('France');
        $company->setEmail('contact@fakturz.fr');
        $company->setPhoneNumber('0156069041');
        $company->setTva(20);
        $company->setOwner($owner);
        $company->setUpdateAt(new \DateTime());
        $manager->persist($company);
        
        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            UserFixtures::class
        ];
    }
}
