<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserFixtures extends Fixture
{
    /** @var UserPasswordHasherInterface $hasher */
    private $hasher;

    /** @var string $pwd */
    private $pwd = '123456';

    public function __construct(UserPasswordHasherInterface $hasher)
    {
        $this->hasher = $hasher;
    }

    public function load(ObjectManager $manager): void
    {
        $faker = \Faker\Factory::create();

        $user = (new User())
            ->setEmail('dev@user')
            ->setRoles([])
            ->setFirstName('user')
            ->setLastName('dev')
            ->setIsVerified(true)
            ->setBirthDay(new \DateTime('2001-06-05 12:15:00'));
        $user->setPassword($this->hasher->hashPassword($user, $this->pwd));
        $manager->persist($user);

        $user = (new User())
            ->setEmail('dev@admin')
            ->setRoles(["ROLE_ADMIN"])
            ->setFirstName('admin')
            ->setLastName('dev')
            ->setIsVerified(true)
            ->setBirthDay(new \DateTime('2001-06-05 12:15:00'));
        $user->setPassword($this->hasher->hashPassword($user, $this->pwd));
        $manager->persist($user);

        for ($i = 0; $i < 10; $i++) {
            $user = (new User())
                ->setEmail($faker->email)
                ->setRoles([])
                ->setFirstName($faker->firstName)
                ->setLastName($faker->lastName)
                ->setIsVerified($faker->boolean())
                ->setBirthDay($faker->dateTime);
            $user->setPassword($this->hasher->hashPassword($user, $this->pwd));
            $manager->persist($user);
        }

        $manager->flush();
    }
}
