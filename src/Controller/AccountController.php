<?php

namespace App\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AccountController extends AbstractController
{
    #[Route('/account', name: 'account', methods: ['GET'])]
    public function index(): Response
    {
        $companyId = null;
        /**
         * @var User
         */
        $user = $this->getUser();
        if ($user !== null) {
            $companyId = $user->getCompany()->getId();
        }
        
        return $this->render('account/index.html.twig', [
            'companyId' => $companyId,
        ]);
    }
}
