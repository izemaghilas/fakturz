<?php

namespace App\Controller;

use App\Entity\Product;
use App\Form\ProductType;
use App\Service\ProductService;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/products', name: 'products_')]
class ProductController extends AbstractController
{
    #[Route('/', name: 'list', methods: ['GET'])]
    public function index(ProductService $productService): Response
    {
        return $this->render('product/index.html.twig', [
            'products' => $productService->readAll()
        ]);
    }

    #[Route('/create', name: 'create', methods: ['GET', 'POST'])]
    public function create(Request $request, ProductService $productService): Response
    {
        $product = new Product();
        $form = $this->createForm(ProductType::class, $product);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $productService->create($product);
            
            return $this->redirectToRoute('products_list');
        }

        return $this->renderForm('product/create.html.twig', [
            'form' => $form
        ]);
    }

    #[Route('/{id}/update', name: 'update', methods: ['GET', 'POST'])]
    public function update(Product $product, Request $request, ProductService $productService): Response
    {
        $form = $this->createForm(ProductType::class, $product);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $productService->update($product);
            return $this->redirectToRoute('products_list');
        }

        return $this->renderForm('product/update.html.twig', [
            'product' => $product,
            'form' => $form
        ]);
    }

    #[Route('/{id}/delete/{token}', name: 'delete', methods: ['GET'])]
    public function delete(Product $product, string $token, ProductService $productService): Response
    {
        if (!$this->isCsrfTokenValid('delete'.$product->getId(), $token)) {
            throw new Exception('Try again!');
        }
        $productService->delete($product);

        return $this->redirectToRoute('products_list');
    }
}
