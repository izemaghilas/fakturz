<?php

namespace App\Controller;

use App\Entity\Company;
use App\Form\CompanyType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CompanyController extends AbstractController
{
    #[Route('/company/{id}', name: 'company_edit', defaults: ['id' => null], methods: ['GET', 'POST'])]
    public function edit(?Company $company, Request $request, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(CompanyType::class, $company);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($company);
            $entityManager->flush();
            
            // set to null
            // to avoid serialization exception
            $company->setLogoFile();
            
            return $this->redirectToRoute('company_edit', ['id'=>$company->getId()]);
        }

        return $this->renderForm('company/edit.html.twig', [
            'form' => $form,
        ]);
    }
}
