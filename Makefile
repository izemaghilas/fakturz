user:
	docker compose exec php bin/console make:user

entity:
	docker compose exec php bin/console make:entity

fixtures:
	docker compose exec php bin/console d:f:l

router:
	docker compose exec php bin/console debug:router 

diff:
	docker compose exec php bin/console make:migrations:diff
	
migration:
	docker compose exec php bin/console make:migration

migrate:
	docker compose exec php bin/console d:mig:migrate